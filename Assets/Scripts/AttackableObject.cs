using UnityEngine;
using UnityEngine.EventSystems;

public class AttackableObject : MonoBehaviour, IPointerClickHandler
{
	private Stats _stats;

	private bool _inPlayerRadius;
	private bool _canAttack;
	private float _attackCd;
	private PlayerMovement _playerMovement;

	private Stats _playerStats;

	private Animator _animator;

	public void OnPointerClick(PointerEventData eventData) {
		if (!_inPlayerRadius)
			return;
		if (_canAttack && _playerMovement.canAttack)
		{
			Debug.Log("Player is attacking");
			_stats.UpdateHealth(_playerStats.GetAttackDamage());
			_attackCd = 0f;
			_canAttack = false;

			// Play attack animation
			_animator.Play("attack");
		}
	}
	
	private void Awake()
	{
		_stats = GetComponent<Stats>();
		_playerStats = GameObject.FindGameObjectWithTag("Player").GetComponent<Stats>();
		_playerMovement	 = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMovement>();
		_animator = GameObject.Find("/Player").GetComponent<Animator>();
	}

	private void Update()
	{
		_attackCd += Time.deltaTime;
		if (_stats.GetAttackSpeed() <= _attackCd)
			_canAttack = true;
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		_inPlayerRadius = true;
	}
	private void OnTriggerStay(Collider other)
	{
		_inPlayerRadius = true;
	}
	private void OnTriggerExit2D(Collider2D collision)
	{
		_inPlayerRadius = false;
	}
}
