using DialogueEditor;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class ClickableObject : MonoBehaviour, IPointerClickHandler, IEventSystemHandler
{
    [Range(1, 10)] [SerializeField] private int _neededClicks = 5;
    [SerializeField] private bool _resetClicks;
    [SerializeField] private GameObject enemy;

    [Range(0f, 20f)] [SerializeField] private float _clickRadius = 1.7f;
    [SerializeField] private CircleCollider2D _triggerCollider;

    [SerializeField] private UnityEvent _onFullClicked;

    private int _currentClicks;
    private bool _inPlayerRadius;

    public void OnPointerClick(PointerEventData eventData) {
        if (!_inPlayerRadius || ConversationManager.Instance.IsConversationActive)
            return;

        if (++_currentClicks >= _neededClicks) {
            _onFullClicked?.Invoke();
            Debug.Log($"{name} clicked");
            if (_resetClicks)
                _currentClicks = 0;
            if (enemy)
            {
	            var transform1 = transform;
	            Instantiate(enemy, transform1.position, transform1.rotation);
            }
        }
    }

    private void Start()    {
        _currentClicks = 0;
        _triggerCollider.radius = _clickRadius;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        _inPlayerRadius = true;
    }
    private void OnTriggerStay(Collider other)
    {
        _inPlayerRadius = true;
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        _inPlayerRadius = false;
    }
}
