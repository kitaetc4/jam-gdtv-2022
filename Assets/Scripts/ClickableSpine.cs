using System;
using DialogueEditor;
using UnityEngine;
using UnityEngine.EventSystems;

public class ClickableSpine : MonoBehaviour, IPointerClickHandler
{
	private bool _inPlayerRadius;
	private PlayerMovement _playerMovement;

	private void Awake()
	{
		_playerMovement = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMovement>();
	}

	public void OnPointerClick(PointerEventData eventData) {
		if (!_inPlayerRadius || ConversationManager.Instance.IsConversationActive)
			return;
		Debug.Log("Here!");
		if (!_playerMovement.HasSpine)
		{
			GetComponent<QuestGoalAchiever>().AchieveGoal();
			_playerMovement.HasSpine = true;
			Destroy(gameObject);
		}
	}
	
	private void OnTriggerEnter2D(Collider2D collision)
	{
		_inPlayerRadius = true;
	}

	private void OnTriggerStay2D(Collider2D other)
	{
		_inPlayerRadius = true;
	}

	private void OnTriggerExit2D(Collider2D collision)
	{
		_inPlayerRadius = false;
	}
}
