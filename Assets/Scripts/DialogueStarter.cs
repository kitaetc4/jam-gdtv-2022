using DialogueEditor;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace DefaultNamespace
{
    public class DialogueStarter : MonoBehaviour
    {
        [SerializeField] private bool _canRestartConversation = false;
        [SerializeField] private NPCConversation _conversation;
        private bool _conversationEnded;

        public UnityEvent OnConversationEnded;

        public void StartConversation() {
            if (_conversation != null && !ConversationManager.Instance.IsConversationActive && !_conversationEnded || _canRestartConversation)
            {
                ConversationManager.Instance.StartConversation(_conversation);
                ConversationManager.OnConversationEnded += FireConversationEnded;
            }
        }

        private void FireConversationEnded()
        {
            _conversationEnded = true;
            OnConversationEnded.Invoke();
            ConversationManager.OnConversationEnded -= FireConversationEnded;
        }
    }
}