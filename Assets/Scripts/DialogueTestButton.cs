using DialogueEditor;
using UnityEngine;
using UnityEngine.UI;

namespace DefaultNamespace
{
    public class DialogueTestButton : MonoBehaviour
    {
        public NPCConversation Dialogue;

        public void Start()
        {
            GetComponent<Button>().onClick.AddListener(() =>
            {
                ConversationManager.Instance.StartConversation(Dialogue);
            });
        }
    }
}