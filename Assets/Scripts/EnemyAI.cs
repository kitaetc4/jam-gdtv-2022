using UnityEngine;
using UnityEngine.AI;

public class EnemyAI : MonoBehaviour
{
	[SerializeField] private float wanderTimer = 5;
	[SerializeField] private float wanderRadius = 5;

	private Stats _stats;
	private Transform _target;
	private float _canAttack;
	private NavMeshAgent _agent;
	private bool _playerInRadius;

	private bool _atPoint;
	private Vector3 _currentPoint;
	private float timer;

	public Animator animator;


	private void Awake()
	{
		_stats = GetComponent<Stats>();
		_agent = GetComponent<NavMeshAgent>();
		_agent.updateRotation = false;
		_agent.updateUpAxis = false;

		timer = wanderTimer;
	}

	private void Update()
	{
		timer += Time.deltaTime;

		if (timer >= wanderTimer)
		{
			if(!RandomPoint(transform.position, wanderRadius, out _currentPoint))
            {
				timer = wanderTimer;
				return;
			}

			_currentPoint.z = 0;
			_agent.SetDestination(_currentPoint);
			timer = 0;
		}
		Debug.DrawLine(transform.position, _currentPoint, Color.black);
	}

	private bool RandomPoint(Vector3 center, float range, out Vector3 result)
	{
		for (int i = 0; i < 30; i++)
		{
			Vector3 randomPoint = center + Random.insideUnitSphere * range;
			NavMeshHit hit;
			if (NavMesh.SamplePosition(randomPoint, out hit, 1.0f, NavMesh.AllAreas))
			{
				result = hit.position;
				return true;
			}
		}
		result = Vector3.zero;
		return false;
	}

	// TODO ������ ���������, ������� ��������� ��� �����, ���� ������� �����
	private void OnCollisionStay2D(Collision2D other)
	{
		if (other.gameObject.CompareTag("Player"))
		{
			if (_stats.GetAttackSpeed() <= _canAttack)
			{
				Debug.Log("Enemy attacking");
				other.gameObject.GetComponent<Stats>().UpdateHealth(_stats.GetAttackDamage());
				_canAttack = 0f;

				// Play attack animation
				animator.Play("attack");
			}
			else
			{
				_canAttack += Time.deltaTime;
			}
		}
	}

	private void OnTriggerEnter2D(Collider2D col)
	{
		if (col.CompareTag("Player"))
		{
			_target = col.transform;
		}
	}
	
	private void OnTriggerExit2D(Collider2D other)
	{
		if (other.CompareTag("Player"))
		{
			_target = null;
		}
	}
	
}
