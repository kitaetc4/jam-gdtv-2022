using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
	[SerializeField] private Image healthBarTotal;
	private Stats _playerStats;

	private void Awake()
	{
		_playerStats = GetComponent<Stats>();
	}

	private void Update()
	{
		healthBarTotal.fillAmount = _playerStats.Health / 100;
	}
}
