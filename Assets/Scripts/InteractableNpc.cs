using System;
using DialogueEditor;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace DefaultNamespace
{
    public class InteractableNpc : MonoBehaviour, IPointerClickHandler, IEventSystemHandler
    {
        [SerializeField] private SpriteRenderer _textHintSprite;
        [SerializeField] private NPCConversation _conversation;

        public UnityEvent OnConversationEnded;

        private bool _inPlayerRadius;

        public void OnPointerClick(PointerEventData eventData) {
            if (!_inPlayerRadius)
                return;

            if (!ConversationManager.Instance.IsConversationActive)
            {
                ConversationManager.Instance.StartConversation(_conversation);
                ConversationManager.OnConversationEnded += FireConversationEnded;
            }
        }

        private void FireConversationEnded()
        {
            OnConversationEnded.Invoke();
            ConversationManager.OnConversationEnded -= FireConversationEnded;
        }

        private void Awake()
        {
            _textHintSprite.gameObject.SetActive(false); 
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            _inPlayerRadius = true;
            _textHintSprite.gameObject.SetActive(true);
        }
        private void OnTriggerStay(Collider other)
        {
            _inPlayerRadius = true;
        }
        private void OnTriggerExit2D(Collider2D collision)
        {
            _inPlayerRadius = false;
            _textHintSprite.gameObject.SetActive(false);
        }
    }
}