using DialogueEditor;
using UnityEngine;
using UnityEngine.EventSystems;

namespace DefaultNamespace
{
    public class MouseHoveredGraphics : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        public SpriteRenderer TargetSprite;
        
        public Color NormalColor;
        public Color HighlihtedColor;
        public Vector3 HighlihtedScale = Vector3.one;
        public bool DoScale = false;
        private bool _isHovered;
        private bool _inPlayerRadius;

        public void Start()
        {
            NormalScale = TargetSprite.transform.localScale;
        }

        public Vector3 NormalScale { get; set; }

        public void Update()
        {
            if (TargetSprite == null) return;
            if (_isHovered && _inPlayerRadius && !ConversationManager.Instance.IsConversationActive)
            {
                TargetSprite.color = HighlihtedColor;
                if (DoScale)
                TargetSprite.transform.localScale = HighlihtedScale;
            }
            else
            {
                TargetSprite.color = NormalColor;
                if (DoScale)
                TargetSprite.transform.localScale = NormalScale;
            }
        }


        public void OnPointerEnter(PointerEventData eventData)
        {
            _isHovered = true;
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            _isHovered = false;
        }
	
        private void OnTriggerEnter2D(Collider2D collision)
        {
            _inPlayerRadius = true;
        }

        private void OnTriggerStay2D(Collider2D other)
        {
            _inPlayerRadius = true;
        }

        private void OnTriggerExit2D(Collider2D collision)
        {
            _inPlayerRadius = false;
        }
    }
}