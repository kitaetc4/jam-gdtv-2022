using UnityEngine;
using DialogueEditor;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] private float moveSpeed = 1f;

    private Rigidbody2D _playerRb;

    private Vector2 _movement;
    public bool HasSpine;
    public bool canAttack;

    public Animator animator;

    public NPCConversation conversation;


    private void Start()
    {
        ConversationManager.Instance.StartConversation(conversation);
    }
    
    private void Awake()
    {
        _playerRb = GetComponent<Rigidbody2D>();
        // HasSpine = true; //temporary
    }
    private void FixedUpdate()
    {
	    _playerRb.velocity = new Vector2(_movement.x * moveSpeed * Time.fixedDeltaTime, _movement.y * moveSpeed * Time.fixedDeltaTime);
    }

    private void Update()
    {
	    if (HasSpine)
	    {
		    _movement.x = Input.GetAxisRaw("Horizontal");
		    _movement.y = Input.GetAxisRaw("Vertical");
	    }

        animator.SetFloat("speed", _movement.sqrMagnitude);

        Vector3 theScale = transform.localScale;
        if (_movement.x < 0 && theScale.x > 0)
        {
            theScale.x *= -1;
        }
        
        if (_movement.x > 0 && theScale.x < 0)
        {
            theScale.x *= -1;
        }
        
        transform.localScale = theScale;
    }
}
