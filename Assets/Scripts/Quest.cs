using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DialogueEditor;

public class Quest
{
    public string title;
    public Dictionary<string, QuestGoal> goals = new Dictionary<string, QuestGoal>();
    public bool isActive = true;
    public bool finished = false;
    public bool withProgress =  false;
    public int progress = 0;

    public GameObject popup;

    public NPCConversation conversation;

    public Quest(string newTitle)
    {
        title = newTitle;
    }

    public void AddGoal(QuestGoal goal)
    {
        if (!goals.ContainsKey(goal.title))
            goals.Add(goal.title, goal);
    }

    public void AddGoalSrt(string title, bool achieved = false)
    {
        QuestGoal goal = new QuestGoal();
        goal.title = title;
        goal.achieved = achieved;
        AddGoal(goal);
    }

    public string ToText()
    {
        string text = "- " + title;

        if (goals.Count > 0)
        {
            text += ":";
        }
        text += "\n";

        foreach (KeyValuePair<string, QuestGoal> entry in goals)
        {
            QuestGoal goal = entry.Value;
            string color = "<color=#FFF>";
            string strikeEnd = "";
            if (goal.achieved) {
                color = "<color=#999><s>";
                strikeEnd = "</s>";
            }

            text += "  " + color + "- " + goal.title + strikeEnd + "<color=#FFF>\n";
        }

        if (withProgress)
        {
            text += "  progress: " + progress + "%\n";
        }

        return text;
    }

    public void AchieveGoal(string goalTitle)
    {
        QuestGoal goal = goals[goalTitle];
        goal.achieved = true;

        // if all goals achieved - finish the quest
        bool allGoalsAchieved = true;
        foreach (KeyValuePair<string, QuestGoal> entry in goals)
        {
            if (!entry.Value.achieved) {
                allGoalsAchieved = false;
            }
        }

        if (allGoalsAchieved)
        {
            finished = true;

            if (conversation)
            {
                ConversationManager.Instance.StartConversation(conversation);
            }
        }
    }

    public void Progress(int increment)
    {
        if (progress < 100)
        {
            progress += increment;
        }
        
        if (progress >= 100)
        {
            finished = true;

            if (conversation)
            {
                ConversationManager.Instance.StartConversation(conversation);
            }

            if (popup)
            {
                popup.SetActive(true);
            }
        }
    }
    

}



public class QuestGoal
{
    public string title;
    public bool achieved = false;
}
