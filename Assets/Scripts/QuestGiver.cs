using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using DialogueEditor;

public class QuestGiver : MonoBehaviour, IPointerClickHandler
{
    private QuestLog questLog;
    private PlayerMovement _playerMovement;

    public NPCConversation conversationForLimbs;

    public GameObject brainAnimation;

    public AudioClip audioLimbs;
    public AudioClip audioBrain;

    private void Awake()
    {
        questLog = GameObject.Find("/Player/QuestLog").GetComponent<QuestLog>();
        _playerMovement	 = GameObject.Find("Player").GetComponent<PlayerMovement>();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        // Quest quest = new Quest("Find limbs");
        // quest.AddGoalSrt("spine", true);
        // quest.AddGoalSrt("left hand");
        // quest.AddGoalSrt("right hand");
        // // quest.AddGoalSrt("left leg");
        // // quest.AddGoalSrt("right leg");

        // questLog.AddQuest(quest);
    }

    public void GiveTheQuest()
    {
        Quest quest = new Quest("Take the spine");
        quest.AddGoalSrt("spine");
        questLog.AddQuest(quest);
    }

    public void GiveQuestLimbs()
    {
        Quest quest = new Quest("Find limbs");
        quest.AddGoalSrt("left hand");
        quest.AddGoalSrt("right hand");
        quest.AddGoalSrt("left leg");
        quest.AddGoalSrt("right leg");
        quest.AddGoalSrt("chest");
        quest.conversation = conversationForLimbs;

        questLog.AddQuest(quest);

        GameObject.Find("/Music").GetComponent<AudioSource>().clip = audioLimbs;
        GameObject.Find("/Music").GetComponent<AudioSource>().Play();
    }

    public void GiveQuestBrain()
    {
	    _playerMovement.canAttack = true;

        Quest quest = new Quest("Collect brain pieces");
        quest.withProgress = true;
        quest.popup = brainAnimation;

        questLog.AddQuest(quest);

        GameObject.Find("/Music").GetComponent<AudioSource>().clip = audioBrain;
        GameObject.Find("/Music").GetComponent<AudioSource>().Play();
    }
}
