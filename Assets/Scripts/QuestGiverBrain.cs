using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class QuestGiverBrain : MonoBehaviour, IPointerClickHandler
{
    private QuestLog questLog;

    private void Awake()
    {
        questLog = GameObject.Find("/Player/QuestLog").GetComponent<QuestLog>();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        Quest quest = new Quest("Collect brain pieces");
        quest.withProgress = true;

        questLog.AddQuest(quest);

        Destroy(gameObject);
    }
}
