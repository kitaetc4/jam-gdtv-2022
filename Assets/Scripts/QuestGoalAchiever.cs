using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using DialogueEditor;

public class QuestGoalAchiever : MonoBehaviour, IPointerClickHandler
{
    [SerializeField] public string questTitle;
    [SerializeField] public string goalTitle;
	[SerializeField] public NPCConversation conversation;

	private QuestLog questLog;
	private bool _inPlayerRadius;

	private void Awake()
    {
        questLog = GameObject.Find("/Player/QuestLog").GetComponent<QuestLog>();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (!_inPlayerRadius) return;

        // questLog.AchieveGoal(questTitle, goalTitle);
		// AchieveGoal();
    }

    public void AchieveGoal()
    {
		questLog.AchieveGoal(questTitle, goalTitle);
		
		if (conversation)
		{
			ConversationManager.Instance.StartConversation(conversation);
		}
    }

	private void OnTriggerEnter2D(Collider2D collision)
	{
		_inPlayerRadius = true;
	}
	private void OnTriggerStay(Collider other)
	{
		_inPlayerRadius = true;
	}
	private void OnTriggerExit2D(Collider2D collision)
	{
		_inPlayerRadius = false;
	}
}
