using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class QuestLog : MonoBehaviour
{
    public TMP_Text textUI;
    private Dictionary<string, Quest> quests = new Dictionary<string, Quest>();

    public void AddQuest(Quest quest) {
        if (!quests.ContainsKey(quest.title))
        {
            quests.Add(quest.title, quest);
            UpdateText();
        }
    }

    private void UpdateText()
    {
        string text = "";

        foreach (KeyValuePair<string, Quest> entry in quests)
        {
            Quest quest = entry.Value;
            if (!quest.finished)
            {
                text += quest.ToText();
            }
        }

        if (text == "")
        {
            text = "Find a quest!";
        }

        textUI.text = text;
    }

    public void AchieveGoal(string questTitle, string goalTitle)
    {
        Quest quest = quests[questTitle];
        quest.AchieveGoal(goalTitle);
        UpdateText();
    }

    public void Progress(string questTitle, int increment)
    {
        Quest quest = quests[questTitle];
        quest.Progress(increment);
        UpdateText();
    }
}
