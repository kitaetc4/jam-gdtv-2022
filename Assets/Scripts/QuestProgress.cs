using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class QuestProgress : MonoBehaviour, IPointerClickHandler
{
	[SerializeField] public string questTitle;
	[SerializeField] public int increment;

	private QuestLog questLog;
	private bool _inPlayerRadius;

	private void Awake()
	{
		questLog = GameObject.Find("/Player/QuestLog").GetComponent<QuestLog>();
	}

	public void OnPointerClick(PointerEventData eventData)
	{
		if (!_inPlayerRadius) return;

		Progress();
	}

	public void Progress()
	{
		questLog.Progress(questTitle, increment);
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		_inPlayerRadius = true;
	}
	private void OnTriggerStay(Collider other)
	{
		_inPlayerRadius = true;
	}
	private void OnTriggerExit2D(Collider2D collision)
	{
		_inPlayerRadius = false;
	}
}
