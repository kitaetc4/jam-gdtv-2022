using DialogueEditor;
using UnityEngine;
using UnityEngine.UI;

namespace DefaultNamespace
{
    public class StartConversationButton : MonoBehaviour
    {
        [SerializeField] private NPCConversation _conversation;

        private void Start()
        {
            GetComponent<Button>().onClick.AddListener(StartConverstion);
        }

        private void StartConverstion()
        {
            ConversationManager.Instance.StartConversation(_conversation);
        }
    }
}