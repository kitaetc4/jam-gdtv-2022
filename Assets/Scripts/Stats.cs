using DialogueEditor;
using UnityEngine;

public class Stats : MonoBehaviour
{
	public float Health { get; private set; }
	[SerializeField] private float maxHealth = 100f;
	[SerializeField] private float attackDamage = 10f;
	[SerializeField] private float attackSpeed = 1f;
	[SerializeField] private NPCConversation conversation;

	private void Awake()
	{
		Health = maxHealth;
	}

	private void Update()
	{
		if (Health <= 0)
		{
			if (name.Contains("Player"))
			{
				GetComponent<PlayerMovement>().HasSpine = false;
				ConversationManager.Instance.StartConversation(conversation);
				Health = maxHealth;
				GetComponent<PlayerMovement>().HasSpine = true;
			}
			else
				Destroy(gameObject);
			
		}
	}

	public void UpdateHealth(float mod)
	{
		Health -= mod;
		if (Health <= 0f)
			Debug.Log($"{name} died");
	}

	public float GetAttackDamage()
	{
		return attackDamage;
	}
	
	public float GetAttackSpeed()
	{
		return attackSpeed;
	}
}
