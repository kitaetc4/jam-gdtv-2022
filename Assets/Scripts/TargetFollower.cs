using UnityEngine;

public class TargetFollower : MonoBehaviour
{
    [SerializeField] private Transform _targetTr;

    private Vector3 _offset;

    private void Start()
    {
        if (_targetTr != null)
            _offset = transform.position - _targetTr.position;
    }

    private void Update()
    {
        if (_targetTr != null)
            transform.position = _targetTr.position + _offset;
    }
}
